package console

import (
	"context"
	"fmt"
	"runtime"
	"strings"
	"time"

	"google.golang.org/api/googleapi"
	"google.golang.org/appengine/log"
)

var AppEngineContextKey = "AppEngineContextKey"
var debugMode = false

func SetDebug(b bool) {
	debugMode = b
}

func IsAppEngineContext(c context.Context) bool {
	return c.Value(&AppEngineContextKey) != nil
}

func Sprintf(format string, args ...interface{}) string {

	if !debugMode {
		return fmt.Sprintf(format+"\n", args...)
	}

	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)

	args = append(args, time.Now(), mem.Alloc/1000/1000)
	return fmt.Sprintf(format+"%v %dmb\n", args...)
}

func Printf(format string, args ...interface{}) {

	if !debugMode {
		fmt.Printf(format+"\n", args...)
		return
	}

	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)

	args = append(args, time.Now(), mem.Alloc/1000/1000)
	fmt.Printf(format+"\t[%v] %dmb\n", args...)
}

func Error(ctx context.Context, err error, args ...string) error {

	tag := ""
	if len(args) > 0 {
		tag = "[" + strings.Join(args, ",") + "]"
	}

	switch e := err.(type) {
	case *googleapi.Error:
		err = fmt.Errorf("%s%s\n%s", tag, e.Error(), e.Body)
	}

	if ctx != nil {

		if IsAppEngineContext(ctx) {

			log.Errorf(ctx, "%s%s", tag, err.Error())
			return err
		}
	}

	fmt.Print("[Error]")
	Printf("%s%s", tag, err.Error())

	return err
}

func Criticalf(ctx context.Context, format string, args ...interface{}) {
	if ctx != nil {
		if IsAppEngineContext(ctx) {
			log.Criticalf(ctx, format, args...)
			return
		}
	} else {
		fmt.Print("[Criticalf]")
		Printf(format, args...)
	}
}
func Debugf(ctx context.Context, format string, args ...interface{}) {
	if ctx != nil {
		if IsAppEngineContext(ctx) {
			log.Debugf(ctx, format, args...)
			return
		}

	} else {
		fmt.Print("[Debugf]")
		Printf(format, args...)
	}
}
func Errorf(ctx context.Context, format string, args ...interface{}) error {
	err := fmt.Errorf(format, args...)
	if ctx != nil {
		if IsAppEngineContext(ctx) {
			log.Errorf(ctx, format, args...)
			return err
		}
	}
	return Error(ctx, err)
}
func Infof(ctx context.Context, format string, args ...interface{}) {
	if ctx != nil {
		if IsAppEngineContext(ctx) {
			log.Infof(ctx, format, args...)
			return
		}

	} else {
		// fmt.Print("[Infof]")
		Printf(format, args...)
	}
}
func Warningf(ctx context.Context, format string, args ...interface{}) {
	if ctx != nil {
		if IsAppEngineContext(ctx) {

			log.Warningf(ctx, format, args...)
			return
		}

	} else {
		fmt.Print("[Warningf]")
		Printf(format, args...)
	}
}
